# Distributed Session
### Tips：
     required: JDK1.7+
     ① distributed-session-1.0-SNAPSHOT-JDK17.jar for JDK1.7
     ② distributed-session-1.0-SNAPSHOT-JDK18.jar for JDK1.8

# ①配置web.xml：

### Ⅰ.配置Filter
    <!-- Filter Chain -->

    <filter>
        <filter-name>DistributedSessionFilter</filter-name>
        <filter-class>com.darkidiot.session.DistributedSessionFilter</filter-class>
        <init-param>
            <param-name>sessionCookieName</param-name>
            <param-value>msid</param-value>
        </init-param>
        <init-param>
            <param-name>cookieDomain</param-name>
            <param-value>www.darkidiot.com</param-value>
        </init-param>
        <init-param>
            <param-name>maxInactiveInterval</param-name>
            <param-value>1800</param-value>
        </init-param>
        <init-param>
            <param-name>cookieContextPath</param-name>
            <param-value>/</param-value>
        </init-param>
        <init-param>
            <param-name>cookieMaxAge</param-name>
            <param-value>-1</param-value>
        </init-param>
    </filter>

    <filter-mapping>
        <filter-name>DistributedSessionFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

ps:移动识别码(mobile station identifier=MSID)

# ②配置session.properties：

##### 配置数据库类型，默认redis
session.source=redis
##### 配置序列化类型，支持 json, binary 默认json
session.serialize.type=json
##### 配置session在redis存放的key前缀
session.redis.prefix=msid
##### 配置是否集群，默认False
session.redis.cluster=false
##### 配置获取连接前是否先测试
session.redis.test.on.borrow=true
##### 配置最大空闲进程数量
session.redis.max.idle=2
##### 配置最大进程数量
session.redis.max.total=5

### 单机配置
##### 配置redis服务器地址
session.redis.host=127.0.0.1
##### 配置redis端口
session.redis.port=6379
##### 配置redis存放的db的index
session.redis.db.index=0


### 集群配置
##### 配置集群监控master名称
session.redis.sentinel.master.name=masterName
##### 配置集群监控服务器地址(多个以逗号分隔)如 192.168.0.1:6379,192.168.0.2:6379
session.redis.sentinel.hosts=10.124.31.19:6379,10.124.31.29:6379

