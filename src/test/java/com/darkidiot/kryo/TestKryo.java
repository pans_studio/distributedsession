package com.darkidiot.kryo;

import com.darkidiot.session.serialize.KryoSerializer;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.CollectionSerializer;
import com.esotericsoftware.kryo.serializers.JavaSerializer;
import com.esotericsoftware.kryo.serializers.MapSerializer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.BaseEncoding;
import lombok.extern.slf4j.Slf4j;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * TestKryo 测试类
 * Kryo 是一个快速高效的Java对象图形序列化框架，主要特点是性能、高效和易用。该项目用来序列化对象到文件、数据库或者网络。
 * Kryo的序列化及反序列速度很快，据说很多大公司都在用。我在把对象序列化都转换成了字符串形式，是为了把对象存储到缓存中。
 * 我们日常项目中使用的数据形式包括对象、List、Set和Map，因此主要把这几种类型的数据进行了序列化及反序列化，支持对象中包含List、Set和Map。
 * Copyright (c) for darkidiot
 * Date:2017/4/16
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
@Slf4j
public class TestKryo {

    private static long time;

    @BeforeClass
    public static void before() {
        time = System.currentTimeMillis();
    }

    @AfterClass
    public static void after() {
        log.info("Spend time:{}", System.currentTimeMillis() - time);
    }

    @Test
    public void testObject() {
        CustomItemDto val = new CustomItemDto();
        val.setId(Long.parseLong(String.valueOf(1)));
        val.setItemCode("");
        val.setItemPrice(32.45);
        val.setItemMemo(null);
        val.setItemName("张金");
        val.setItemPrice(89.02);
        val.setSort(10);
        String a = serializationObject(val);
        CustomItemDto newValue = deserializationObject(a, CustomItemDto.class);
        Assert.assertEquals(val.getId(), newValue.getId());
    }

    @Test
    public void testList() {
        List<CustomItemDto> lst = Lists.newArrayList();
        for (int i = 0; i < 10; i++) {
            CustomItemDto val = new CustomItemDto();
            val.setId(Long.parseLong(String.valueOf(i)));
            val.setItemCode("");
            val.setItemPrice(32.45);
            val.setItemMemo(null);
            val.setItemName("张金");
            val.setItemPrice(89.02);
            val.setSort(10);
            lst.add(val);
        }

        String a = serializationList(lst, CustomItemDto.class);
        List<CustomItemDto> newValue = deserializationList(a,
                CustomItemDto.class);
        Assert.assertEquals(lst.size(), newValue.size());
    }

    @Test
    public void testBean() {
        List<CustomCategoryDto> lst = Lists.newArrayList();
        for (int j = 0; j < 10; j++) {
            CustomCategoryDto dto = new CustomCategoryDto();
            dto.setCategoryCode("ABCD_001");
            dto.setCategoryName("呼吸系统");
            for (int i = 0; i < 10; i++) {
                CustomItemDto val = new CustomItemDto();
                val.setId(Long.parseLong(String.valueOf(i)));
                val.setItemCode("");
                val.setItemPrice(32.45);
                val.setItemMemo(null);
                val.setItemName("张金");
                val.setItemPrice(89.02);
                val.setSort(10);
                dto.getCustomItemList().add(val);
            }
            for (int i = 0; i < 10; i++) {
                CustomItemDto val = new CustomItemDto();
                val.setId(Long.parseLong(String.valueOf(i)));
                val.setItemCode("");
                val.setItemPrice(32.45);
                val.setItemMemo(null);
                val.setItemName("张金");
                val.setItemPrice(89.02);
                val.setSort(10);
                dto.getCustomItemSet().add(val);
            }
            for (int i = 0; i < 10; i++) {
                CustomItemDto val = new CustomItemDto();
                val.setId(Long.parseLong(String.valueOf(i)));
                val.setItemCode("");
                val.setItemPrice(32.45);
                val.setItemMemo(null);
                val.setItemName("张金");
                val.setItemPrice(89.02);
                val.setSort(10);
                dto.getCustomItemMap().put(String.valueOf(i), val);
            }
            lst.add(dto);
        }

        String a = serializationList(lst, CustomCategoryDto.class);
        List<CustomCategoryDto> newValue = deserializationList(a, CustomCategoryDto.class);
        Assert.assertEquals(lst.size(), newValue.size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testMap() {
        Map<String, CustomItemDto> map = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            CustomItemDto val = new CustomItemDto();
            val.setId(Long.parseLong(String.valueOf(i)));
            val.setItemCode("");
            val.setItemPrice(32.45);
            val.setItemMemo(null);
            val.setItemName("张金");
            val.setItemPrice(89.02);
            val.setSort(10);
            map.put(val.hashCode() + "", val);
        }

        String a = serializationMap(map, CustomItemDto.class);
        Map<String, CustomItemDto> newValue = deserializationMap(a, CustomItemDto.class);
        Assert.assertEquals(map.size(), newValue.size());

        KryoSerializer kryoSerializer = new KryoSerializer();
        Map con = Maps.newHashMap();
        con.put("userId", 39036);
        String serialize = kryoSerializer.serialize(con);
        log.info(serialize);
        Map<String, Object> deserialize = kryoSerializer.deserialize(serialize);
        log.info(deserialize.toString());

    }

    @Test
    public void testSet() {
        Set<CustomItemDto> set = Sets.newHashSet();
        for (int i = 0; i < 10; i++) {
            CustomItemDto val = new CustomItemDto();
            val.setId(Long.parseLong(String.valueOf(i)));
            val.setItemCode("");
            val.setItemPrice(32.45);
            val.setItemMemo(null);
            val.setItemName("金星");
            val.setItemPrice(89.02);
            val.setSort(10);
            set.add(val);
        }

        String a = serializationSet(set, CustomItemDto.class);
        Set<CustomItemDto> newValue = deserializationSet(a, CustomItemDto.class);
        Assert.assertEquals(set.size(), newValue.size());
    }

    private <T extends Serializable> String serializationObject(T obj) {
        Kryo kryo = new Kryo();
        kryo.setReferences(false);
        kryo.register(obj.getClass(), new JavaSerializer());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Output output = new Output(baos);
        kryo.writeClassAndObject(output, obj);
        output.flush();
        output.close();

        byte[] b = baos.toByteArray();
        try {
            baos.flush();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return BaseEncoding.base16().encode(b);
    }

    @SuppressWarnings("unchecked")
    private <T extends Serializable> T deserializationObject(String obj, Class<T> clazz) {
        Kryo kryo = new Kryo();
        kryo.setReferences(false);
        kryo.register(clazz, new JavaSerializer());

        ByteArrayInputStream bais = new ByteArrayInputStream(BaseEncoding.base16().decode(obj));
        Input input = new Input(bais);
        return (T) kryo.readClassAndObject(input);
    }

    private <T extends Serializable> String serializationList(List<T> obj, Class<T> clazz) {
        Kryo kryo = new Kryo();
        kryo.setReferences(false);
        kryo.setRegistrationRequired(true);

        CollectionSerializer serializer = new CollectionSerializer();
        serializer.setElementClass(clazz, new JavaSerializer());
        serializer.setElementsCanBeNull(false);

        kryo.register(clazz, new JavaSerializer());
        kryo.register(ArrayList.class, serializer);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Output output = new Output(baos);
        kryo.writeObject(output, obj);
        output.flush();
        output.close();

        byte[] b = baos.toByteArray();
        try {
            baos.flush();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return BaseEncoding.base16().encode(b);
    }

    @SuppressWarnings("unchecked")
    private <T extends Serializable> List<T> deserializationList(String obj, Class<T> clazz) {
        Kryo kryo = new Kryo();
        kryo.setReferences(false);
        kryo.setRegistrationRequired(true);

        CollectionSerializer serializer = new CollectionSerializer();
        serializer.setElementClass(clazz, new JavaSerializer());
        serializer.setElementsCanBeNull(false);

        kryo.register(clazz, new JavaSerializer());
        kryo.register(ArrayList.class, serializer);

        ByteArrayInputStream bais = new ByteArrayInputStream(BaseEncoding.base16().decode(obj));
        Input input = new Input(bais);
        return (List<T>) kryo.readObject(input, ArrayList.class, serializer);
    }

    private <T extends Serializable> String serializationMap(Map<String, T> obj, Class<T> clazz) {
        Kryo kryo = new Kryo();
        kryo.setReferences(false);
        kryo.setRegistrationRequired(true);

        MapSerializer serializer = new MapSerializer();
        serializer.setKeyClass(String.class, new JavaSerializer());
        serializer.setKeysCanBeNull(false);
        serializer.setValueClass(clazz, new JavaSerializer());
        serializer.setValuesCanBeNull(true);

        kryo.register(clazz, new JavaSerializer());
        kryo.register(HashMap.class, serializer);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Output output = new Output(baos);
        kryo.writeObject(output, obj);
        output.flush();
        output.close();

        byte[] b = baos.toByteArray();
        try {
            baos.flush();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return BaseEncoding.base16().encode(b);
    }

    @SuppressWarnings("unchecked")
    private <T extends Serializable> Map<String, T> deserializationMap(String obj, Class<T> clazz) {
        Kryo kryo = new Kryo();
        kryo.setReferences(false);
        kryo.setRegistrationRequired(true);

        MapSerializer serializer = new MapSerializer();
        serializer.setKeyClass(String.class, new JavaSerializer());
        serializer.setKeysCanBeNull(false);
        serializer.setValueClass(clazz, new JavaSerializer());
        serializer.setValuesCanBeNull(true);

        kryo.register(clazz, new JavaSerializer());
        kryo.register(HashMap.class, serializer);

        ByteArrayInputStream bais = new ByteArrayInputStream(
                BaseEncoding.base16().decode(obj));
        Input input = new Input(bais);
        return (Map<String, T>) kryo.readObject(input, HashMap.class,
                serializer);
    }

    private static <T extends Serializable> String serializationSet(Set<T> obj, Class<T> clazz) {
        Kryo kryo = new Kryo();
        kryo.setReferences(false);
        kryo.setRegistrationRequired(true);

        CollectionSerializer serializer = new CollectionSerializer();
        serializer.setElementClass(clazz, new JavaSerializer());
        serializer.setElementsCanBeNull(false);

        kryo.register(clazz, new JavaSerializer());
        kryo.register(HashSet.class, serializer);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Output output = new Output(baos);
        kryo.writeObject(output, obj);
        output.flush();
        output.close();

        byte[] b = baos.toByteArray();
        try {
            baos.flush();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return BaseEncoding.base16().encode(b);
    }

    @SuppressWarnings("unchecked")
    private static <T extends Serializable> Set<T> deserializationSet(String obj, Class<T> clazz) {
        Kryo kryo = new Kryo();
        kryo.setReferences(false);
        kryo.setRegistrationRequired(true);

        CollectionSerializer serializer = new CollectionSerializer();
        serializer.setElementClass(clazz, new JavaSerializer());
        serializer.setElementsCanBeNull(false);

        kryo.register(clazz, new JavaSerializer());
        kryo.register(HashSet.class, serializer);

        ByteArrayInputStream bais = new ByteArrayInputStream(
                BaseEncoding.base16().decode(obj));
        Input input = new Input(bais);
        return (Set<T>) kryo.readObject(input, HashSet.class, serializer);
    }
}